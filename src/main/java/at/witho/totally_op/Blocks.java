package at.witho.totally_op;

import at.witho.totally_op.block.PeacefulDoubleFlower;
import at.witho.totally_op.block.PeacefulFlower;

public class Blocks
{
    public static PeacefulFlower peaceful_flower;
    public static PeacefulDoubleFlower peaceful_double_flower;

    public static void init()
    {
        peaceful_flower = new PeacefulFlower("peaceful_flower");
        peaceful_double_flower = new PeacefulDoubleFlower("peaceful_double_flower");
    }

}
