package at.witho.totally_op;

import at.witho.totally_op.item.PeacefulTool;
import at.witho.totally_op.item.RoughTool;

public class Items
{
    public static PeacefulTool peaceful_wood_tool;
    public static PeacefulTool peaceful_iron_tool;
    public static PeacefulTool peaceful_diamond_tool;
    public static RoughTool rough_tool;

    public static void init()
    {
    	peaceful_wood_tool = new PeacefulTool( "peaceful_wood_tool", 2, 1);
    	peaceful_iron_tool = new PeacefulTool( "peaceful_iron_tool", 4, 2);
		peaceful_diamond_tool = new PeacefulTool( "peaceful_diamond_tool", 8, 3);
        rough_tool = new RoughTool("rough_tool");
    }

}
