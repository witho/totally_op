package at.witho.totally_op;

import net.fabricmc.api.ModInitializer;
import net.minecraft.block.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ToolMaterial;
import net.minecraft.item.ToolMaterials;

public class TotallyOP implements ModInitializer
{
    public static final String MODID = "totally_op";

    @Override
    public void onInitialize() {
        Blocks.init();
        Items.init();
    }
}
