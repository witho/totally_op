package at.witho.totally_op.block;

import at.witho.totally_op.Blocks;
import at.witho.totally_op.TotallyOP;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.util.Identifier;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.BlockView;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

import java.util.Random;

public class PeacefulFlower extends Block implements Fertilizable {

	public PeacefulFlower(String name) {
		super(Material.PLANTS, Material.PLANTS.getMaterialMapColor());
        //this.setTickRandomly(false);
        this.setSoundType(SoundType.PLANT);
        setHardness(0F);
        Registry.BLOCK.register(new Identifier(TotallyOP.MODID, name), this);
	}
	
	@SideOnly(Side.CLIENT)
    public void initModel() {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }

    private boolean isOre(Block block, String name) {
        NonNullList<ItemStack> blocks = OreDictionary.getOres(name);
        for (ItemStack input : blocks)
        {
            if (Block.getBlockFromItem(input.getItem()) == block) return true;
        }
        return false;
    }

    @Override
    public void randomTick(BlockState state, World world, BlockPos pos, Random rand)
    {
        super.randomTick(state, world, pos, rand);
        BlockState below = world.getBlockState(pos.down());
        Block block = below.getBlock();
        if (block == net.minecraft.block.Blocks.COBBLESTONE) return;
        ItemStack smeltingResult = FurnaceRecipes.instance().getSmeltingResult(new ItemStack(block, 1, block.getMetaFromState(below)));
        ItemStack result = null;
        if (!smeltingResult.isEmpty()) {
            result = smeltingResult.copy();
            result.setAmount(result.getCount() * 9);
        } else {
            if (isOre(block, "blockCoal") || isOre(block, "blockCharcoal")) {
                //result = new ItemStack(Items.diamond_fragment);
            }
            else if (isOre(block, "blockLead")) {
                result = new ItemStack(Items.GOLD_INGOT);
            }
        }
        if (result != null && !result.isEmpty()) {
            world.setBlockState(pos.down(), net.minecraft.block.Blocks.COBBLESTONE.getDefaultState());
            world.setBlockState(pos, net.minecraft.block.Blocks.AIR.getDefaultState());
            world.spawnEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), result));
        }
    }


    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
    {
        IBlockState soil = worldIn.getBlockState(pos.down());
        return soil.getBlock() != net.minecraft.block.Blocks.AIR;
    }

	@Override
    public boolean canBlockStay(World worldIn, BlockPos pos, IBlockState state) {
		return canPlaceBlockAt(worldIn, pos);
	}


    @Override
    public boolean canGrow(World world, BlockPos blockPos, IBlockState iBlockState, boolean b) {
    }

    @Override
    public boolean canUseBonemeal(World world, Random random, BlockPos blockPos, IBlockState iBlockState) {
        return canGrow(world, blockPos, iBlockState, false);
    }

    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return FULL_BLOCK_AABB;
    }

    @Override
    public boolean isFertilizable(BlockView view, BlockPos pos, BlockState state, boolean var4) {
        return world.isAirBlock(blockPos.up());
    }

    @Override
    public boolean canGrow(World world, Random var2, BlockPos pos, BlockState state) {
        return world.isAir(pos.up());
    }

    @Override
    public void grow(World var1, Random var2, BlockPos var3, BlockState var4) {
        world.setBlockState(blockPos, Blocks.peaceful_double_flower.getDefaultState().withProperty(BlockDoublePlant.HALF, BlockDoublePlant.EnumBlockHalf.LOWER));
        world.setBlockState(blockPos.up(), Blocks.peaceful_double_flower.getDefaultState().withProperty(BlockDoublePlant.HALF, BlockDoublePlant.EnumBlockHalf.UPPER));
    }
}
